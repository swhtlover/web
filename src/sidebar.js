import React from "react";
import { BrowserRouter as Router, Route, Link, Switch, Redirect} from "react-router-dom";
import { slide as Menu } from "react-burger-menu";

export default Sidebar => {
  return (
    <Menu {...Sidebar}>
      <a className="menu-item" href="/">
        Home
      </a>

      <a className="menu-item" href="/routes">
        Routes
      </a>

      <a className="menu-item" href="/about">
        About us
      </a>
    </Menu>
  );
};
